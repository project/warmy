<div class="node<?php if (!page&&$zebra){ print " ".$zebra; } if ($sticky) { ?>  sticky<?php } ?><?php if (!$status) { ?> node-unpublished<?php } ?>" id="node-<?php print $node->nid ?>">
		<?php if ($picture) {
      print $picture;
    }?>
		<?php if (!$page) { ?>
		<h2 class="title">
				<a href="<?php print $node_url?>">
						<?php print $title?>
				</a>
		</h2>
		<?php }; ?>
		<?php if ($submitted) { ?>
		<span class="submitted"><?php print $submitted; ?></span>
		<?php } ?>
		<?php if ($terms) { ?>
		<span class="taxonomy"><?php print $terms?></span>
		<?php } ?>
		<div class="content">
				<?php print $content?>
		</div>

				<?php print $links?>

</div>
