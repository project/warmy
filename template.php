<?php

/**
* IE detector & css return 
* Useful for code cleanup for decent browsers. Contains 2 functions 
* phptemplate_ie_detect() - returns TRUE or FALSE - good for further proceeding
* phptemplate_css_iefix() - returns path to ie fix css file raltive to theme root
* params: IE6, IE7, IE All 
* 
*/
function phptemplate_ie_detect() {
    if (isset($_SERVER['HTTP_USER_AGENT'])) { 
    return strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') ? TRUE : FALSE;
		}
		else {return TRUE;}		
}

function phptemplate_css_iefix ($ie6=FALSE, $ie7=FALSE, $ieall=FALSE) {
if (phptemplate_ie_detect()) {
$output = "";
if ($ieall) {
$output .='<!--[if IE]><style type="text/css" media="all">@import "'. base_path() . path_to_theme() .'/'. $ieall .'";</style><![endif]-->';
}
if ($ie6) {
$output .='<!--[if lte IE 6]><style type="text/css" media="all">@import "'. base_path() . path_to_theme() .'/'. $ie6 .'";</style><![endif]-->';
}
if ($ie7) {
$output .='<!--[if lte IE 7]><style type="text/css" media="all">@import "'. base_path() . path_to_theme() .'/'. $ie7 .'";</style><![endif]-->';
}
return $output;
}
return FALSE;
}

/**
* Use this function to add javascripts to your theme correctly
*/

drupal_add_js(path_to_theme().'/js/theme.scripts.js', 'theme');

/**
* Breadcrumb stuff

function generic_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    $breadcrumb[] = drupal_get_title();
   
  }
  else {$breadcrumb[]=t('Home');}
  
   return '<ul class="breadcrumb"><li>'. implode(' &raquo; ', $breadcrumb) .'</li></ul>';
}
*/

/**
* Altering search form


function phptemplate_search_theme_form($form) {
$form['#prefix']='<div id="search" class="container-inline">';
$form['#suffix']='</div>';
unset($form['search_theme_form_keys']['#title']);
$form['submit']['#value']="";
$form['search_theme_form_keys']['#value']="search...";
$form['search_theme_form_keys']['#attributes'] = array(
    'onblur' => 'if(this.value=="") this.value="search...";',
	'onfocus' => 'if(this.value=="search...") this.value="";'
    
  );

 
return drupal_render($form);
}
*/

/**
* Altering search form

function generic_search_item($item, $type) {
  $output = ' <dt class="title"><a href="'. check_url($item['link']) .'">'. check_plain($item['title']) .'</a></dt>';
  $output .= ' <dd>'. ($item['snippet'] ? '<p>'. $item['snippet'] .'</p>' : '');
  return $output;
}
 */
 
/**
 * footer message function
 */
function warmy_footer($message){
  $message.='<div id="author_info">
              <p>Powered by <a href="http://drupal.org/">Drupal</a></p>
			  <p>Designed by <a href="http://www.sfordela.com">sFordela</a></p>
			</div>';
    return $message;
}

