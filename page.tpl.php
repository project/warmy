<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<title><?php print $head_title ?></title>
<?php print $head ?><?php print $styles ?>
<?php 
/**
* this part is returned for IE browsers only. 3 arguments are possible in the following order: path to ie6, ie7 and ie all
* this method is used mostly to clean up code a bit and do not return unnecessary merkup for decent browsers
**/
print phptemplate_css_iefix('ie6-fix.css', 'ie7-fix.css');
?>

<?php print $scripts ?>

<!--[if IE 6]>
<script src="/sites/all/themes/warmy/js/DD_belatedPNG_0.0.7a.js"></script>
<script>
  /* EXAMPLE */
  DD_belatedPNG.fix('#logo img, #header, #content, #footer, h1, #searchbox .form-item, #search .form-submit, #primary-links, .block, #center, #footer_inner, .block-title');
  
  /* string argument can be any CSS selector */
  /* .png_bg example is unnecessary */
  /* change it to what suits you! */
</script>

<script type="text/javascript">
        /* Script to enable hover function */
       $().ready(function() {
              $('#primary-links ul li').hover(
                   function() {
                       $(this).addClass('hovered');
              }, function() {
                    $(this).removeClass('hovered');
              }
           );
       });
</script>
<![endif]--> 

<!--[if IE 7]>
<script src="/sites/all/themes/warmy/js/DD_belatedPNG_0.0.7a.js"></script>
<script>
  /* EXAMPLE */
  DD_belatedPNG.fix('#content');
  
  /* string argument can be any CSS selector */
  /* .png_bg example is unnecessary */
  /* change it to what suits you! */
</script>
<![endif]--> 
</head>
<body class="<?php print $body_classes; ?>">
<div id="wrapper">
  <div id="header">
    <?php print $header; ?>
	<?php if ($logo) { 
	  print l('<img src="'.$logo.'" alt="'.t('Home').'" />', '<front>', array('attributes' => array('id'=>'logo'), 'html'=>'TRUE'));
     }?>
     <div id="sitename"> 
     <h1><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name; ?></a></h1></div>
    <div id="searchbox">  
	  <?php print $search_box ?>
	</div>
  </div>
  <div id="content">
      <?php if (isset($primary_links)) { ?>
  <div id="primary-links">
    <?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'primary')) ?>
  </div>
  <?php } ?>
  <?php if (isset($secondary_links)) { ?>
    <?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'secondary')) ?>
  <?php } ?>
  <div id="left" class="column">
    <?php  print $left;  ?>
  </div>
  <div id="center" class="column">
    <?php if ($title) {  ?>
	  <h1 class="title"><?php print $title ?></h1>
    <?php } ?>
    <div id="wrap">
	<?php if ($tabs) { ?>
	  <div class="tabs">
		<?php print $tabs ?>
	  </div>
	<?php } ?>
	<?php print $breadcrumb ?>
	<?php print $help ?>
	<?php print $messages ?><?php print $content; ?>
	</div>
  </div>
  <div class="clear"></div>
  </div>  

  <div id="footer">
    <div id="footer_inner"> 
       <?php print $footer; ?>
         <div id="primary-links-footer">
           <?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'primary-footer')) ?>
         </div>
       <?php print warmy_footer($message); ?>
    </div>
  </div>
</div>
<?php print $closure ?>
</body>
</html>
